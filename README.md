# Mind Future Workshop: Collaborative Artworks From Packet Captures


## Introduction


Today we will be creating an artwork together from a packet capture in p5js.

The packet capture we're using, is a small cutout from the DEF CON 26 ctf.

Every 30 minutes, you will have to pass your branch to another person, and continue from someone elses branch.

The packet capute can be found in `app/pcap.js` in JSON format. And your p5js visualizaton code in `app/sketch.js`

See the branches and what others are working on [here](https://mindfuture.gitlab.io/bornhack-2020/)


## Getting started


Request access to the repository by clicking the `Request Access` button.

Clone the repository   
```
git clone git@gitlab.com:artificialmind/bornhack-2020.git
```


Your branch name should be given to you on a paper note. 
> It is important that you name your branch corretly, as it will be passed on to one of your peers later on.

Make a new branch
```
git checkout -b <branch_name>
```


Make your changes to `app/sketch`, see [p5.js docs](https://p5js.org/reference/)

Test your app by opening `app/index.html` with your favorite browser.


Deploy your app with 
```
git commit -m "changed things"
git push origin <branch_name>
```

Access your page on `https://mindfuture.gitlab.io/bornhack-2020/<branch_name>/`

See what other people are working on [here](https://mindfuture.gitlab.io/bornhack-2020/)


